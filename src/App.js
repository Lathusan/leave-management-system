import React from 'react';
import Navigationbar from './components/Navigationbar'
import SideNav from './components/SideNav'
import Running from './components/Dashboard'
import { BrowserRouter, Route} from 'react-router-dom'
import Employee from './components/Employee'
import AddPerson from './components/AddPerson';
import Company from './components/Company';
import Calendar from './components/Calendar';
import Leave from './components/Leave';
import Review from './components/Reviews';
import NewCalendar from './components/NewCalendar';
import Dashboard from './components/Dashboard';
import Setting from './components/Setting';
import ProfileEmployment from './components/ProfileEmployment';
import Detail from './components/Detail';
import Documents from './components/Documents';
import Payroll from './components/Payroll';
import TimeOff from './components/TimeOff';
import InvictaEmployee from './components/InvictaEmployee';

function App() {
  return (
    <div class="inner-wrapper">

      <div id="loader-wrapper">
				
				<div class="loader">
				  <div class="dot"></div>
				  <div class="dot"></div>
				  <div class="dot"></div>
				  <div class="dot"></div>
				  <div class="dot"></div>
				</div>

			</div>

      <header class="header">
     
      <Navigationbar/>

      </header>

      <div class="page-wrapper">
				<div class="container-fluid">
					<div class="row">

            <BrowserRouter>
              <SideNav/>

              <Route path='/dashboard' component={Dashboard}/>

              <Route path='/employee' component={Employee}/>

              <Route path='/company' component={Company}/>

              <Route path='/leave' component={Leave}/>

              <Route path='/reviews' component={Review}/>

              <Route path='/addperson' component={AddPerson}/>

              <Route path='/setting' component={Setting}/>

              <Route path='/profileemployment' component={ProfileEmployment}/>

              <Route path='/detail' component={Detail}/>

              <Route path='/documents' component={Documents}/>

              <Route path='/payroll' component={Payroll}/>

              <Route path='/timeoff' component={TimeOff}/>
              
              <Route path='/invictaemployee' component={InvictaEmployee}/>

              </BrowserRouter>
  
          </div>
        </div>
      </div>

    </div>
  );
}

export default App;