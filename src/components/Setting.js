import React from 'react'

function Setting() {
    return (

        <div>
            
            <div class="row">
                <div class="col-lg-6 d-flex">
                    <div class="card ctm-border-radius shadow-sm grow flex-fill">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Change Password</h4>
                            <span class="ctm-text-sm">Your password needs to be at least 8 characters long.</span>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Current Password" />
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="New Password" id="pwd" />
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Repeat Password" />
                            </div>
                            <div class="text-center">
                                <a href="javascript:void(0)" class="btn btn-theme button-1 ctm-border-radius text-white text-center">Change My Password</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 d-flex">
                    <div class="card reminder ctm-border-radius shadow-sm grow flex-fill">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Company Notification Settings</h4>
                            <span class="ctm-text-sm">You will receive information across the whole company.</span>
                        </div>
                        <div class="card-body">
                            <div class="custom-control custom-checkbox mb-3">
                                <input type="checkbox" class="custom-control-input" id="weekly-digest" checked />
                                <label class="custom-control-label" for="weekly-digest"><span class="h6">Weekly Summarize</span><br/><span class="ctm-text-sm">Keeping you in the loop with a weekly email summarizing</span> </label>
                            </div>
                            <div class="custom-control custom-checkbox mb-3">
                                <input type="checkbox" class="custom-control-input" id="weekly-payroll" checked />
                                <label class="custom-control-label" for="weekly-payroll"><span class="h6">Weekly Payroll Summarize</span><br/><span class="ctm-text-sm">A weekly email containing all changes related to your payroll..</span></label>
                            </div>
                            <div class="custom-control custom-checkbox mb-3">
                                <input type="checkbox" class="custom-control-input" id="Key" checked />
                                <label class="custom-control-label" for="Key"><span class="h6">Visa Dates</span><br/><span class="ctm-text-sm">Informs and notify the day before Visa dates for each team member.</span></label>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-theme button-1 ctm-border-radius text-white">Update Notification Settings</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="card ctm-border-radius shadow-sm grow">
                    <div class="card-header">
                        <h4 class="card-title mb-0">Team Member Notification Settings</h4>
                        <p class="ctm-text-sm">You will receive notifications only for Team Leads.</p>
                    </div>
                    <div class="card-body">
                        <div class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" class="custom-control-input" id="birthday" checked />
                            <label class="custom-control-label" for="birthday"><span class="h6">Birthdays</span><br/><span class="ctm-text-sm">Reasons to party with reminders a week and a day before a team member’s birthday.</span></label>
                        </div>
                        <div class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" class="custom-control-input" id="Work" checked />
                            <label class="custom-control-label" for="Work"><span class="h6">Work Anniversaries</span><br/><span class="ctm-text-sm">Never miss work anniversaries with reminders the week and the day before.</span></label>
                        </div>
                        <div class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" class="custom-control-input" id="Key1" checked />
                            <label class="custom-control-label" for="Key1"><span class="h6">Key Dates</span><br/><span class="ctm-text-sm">Informs and notify the day before key dates for each team member.</span></label>
                        </div>
                        <div class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" class="custom-control-input" id="Offboardings" checked />
                            <label class="custom-control-label" for="Offboardings"><span class="h6">Off Boardings</span><br/><span class="ctm-text-sm">Informs you when a team member has a leaving date set and reminds you the day before.</span></label>
                        </div>
                        <div class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" class="custom-control-input" id="Home" checked />
                            <label class="custom-control-label" for="Home"><span class="h6">Work From Home Notifications</span><br/><span class="ctm-text-sm">Never miss a notification that someone will be working from home.</span></label>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-theme button-1 ctm-border-radius text-white">Update Notification Settings</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        

        <div class="row">
            <div class="col-md-6 d-flex">
                <div class="card flex-fill ctm-border-radius shadow-sm grow">
                    <div class="card-header">
                        <h4 class="card-title mb-0">
                            Company Default <a href="javascript:void(0)" class="float-right text-primary" data-toggle="modal" data-target="#edit_timedefault"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-6 allowance text-center">
                                <p class="mb-2 btn btn-theme text-white ctm-border-radius">25 days</p>
                                <p class="mb-2 h6">Allowance</p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-6 allowance text-center">
                                <p class="mb-2 btn btn-theme text-white ctm-border-radius">01 January</p>
                                <p class="mb-2 h6">Year Start</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 d-flex">
                <div class="card flex-fill ctm-border-radius shadow-sm grow">
                    <div class="card-header">
                        <h4 class="card-title mb-0">
                            <span>Working Week</span><a href="javascript:void(0)" class="float-right text-primary" data-toggle="modal" data-target="#addWorkWeek"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        </h4>
                        <span class="ctm-text-sm">Set the dates that your company works.</span>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <span class="badge custom-badge badge-primary">Mon</span>
                                <span class="badge custom-badge badge-primary">Tue</span>
                                <span class="badge custom-badge badge-primary">Wed</span>
                                <span class="badge custom-badge badge-primary">Thu</span>
                                <span class="badge custom-badge badge-primary">Fri</span>
                                <span class="badge custom-badge">Sat</span>
                                <span class="badge custom-badge">Sun</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        

        </div>
        
        );
    }
    
export default Setting;
