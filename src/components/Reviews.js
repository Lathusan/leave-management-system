import React from 'react'

function Review() {
    return (			
        <div>
			<div class="page-wrapper">
				<div class="container-fluid">
					<div class="row">


						<div class="col-xl-3 col-lg-4 col-md-12 theiaStickySidebar">
							<aside class="sidebar sidebar-user">
								<div class="card ctm-border-radius shadow-sm grow">
									<div class="card-body py-4">
										<div class="row">
											<div class="col-md-12 mr-auto text-left">
												<div class="custom-search input-group">
													<div class="custom-breadcrumb">
														<ol class="breadcrumb no-bg-color d-inline-block p-0 m-0 mb-2">
															<li class="breadcrumb-item d-inline-block"><a href="index.html" class="text-dark">Home</a></li>
															<li class="breadcrumb-item d-inline-block active">Reviews</li>
														</ol>
														<h4 class="text-dark">Reviews</h4>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="sidebar-wrapper d-lg-block d-md-none d-none">
									<div class="card ctm-border-radius shadow-sm grow border-none">
										<div class="card-body">
											<div class="row no-gutters">
												<div class="col-6 align-items-center text-center">
													<a href="index.html" class="text-dark p-4 first-slider-btn ctm-border-right ctm-border-left ctm-border-top"><span class="lnr lnr-home pr-0 pb-lg-2 font-23"></span><span class="">Dashboard</span></a>												
												</div>
												<div class="col-6 align-items-center shadow-none text-center">											
													<a href="employees.html" class="text-dark p-4 second-slider-btn ctm-border-right ctm-border-top"><span class="lnr lnr-users pr-0 pb-lg-2 font-23"></span><span class="">Employees</span></a>												
												</div>
												<div class="col-6 align-items-center shadow-none text-center">												
													<a href="company.html" class="text-dark p-4 ctm-border-right ctm-border-left"><span class="lnr lnr-apartment pr-0 pb-lg-2 font-23"></span><span class="">Company</span></a>												
												</div>
												<div class="col-6 align-items-center shadow-none text-center">												
													<a href="calendar.html" class="text-dark p-4 ctm-border-right"><span class="lnr lnr-calendar-full pr-0 pb-lg-2 font-23"></span><span class="">Calendar</span></a>												
												</div>
												<div class="col-6 align-items-center shadow-none text-center">											
													<a href="leave.html" class="text-dark p-4 ctm-border-right ctm-border-left"><span class="lnr lnr-briefcase pr-0 pb-lg-2 font-23"></span><span class="">Leave</span></a>											
												</div>												
												<div class="col-6 align-items-center shadow-none text-center">											
													<a href="reviews.html" class="text-white active p-4 ctm-border-right"><span class="lnr lnr-star pr-0 pb-lg-2 font-23"></span><span class="">Reviews</span></a>												
												</div>
												<div class="col-6 align-items-center shadow-none text-center">												
													<a href="reports.html" class="text-dark p-4 ctm-border-right ctm-border-left"><span class="lnr lnr-rocket pr-0 pb-lg-2 font-23"></span><span class="">Reports</span></a>												
												</div>
												<div class="col-6 align-items-center shadow-none text-center">												
													<a href="manage.html" class="text-dark p-4 ctm-border-right"><span class="lnr lnr-sync pr-0 pb-lg-2 font-23"></span><span class="">Manage</span></a>												
												</div>
												<div class="col-6 align-items-center shadow-none text-center">											
													<a href="settings.html" class="text-dark p-4 last-slider-btn1 ctm-border-right ctm-border-left"><span class="lnr lnr-cog pr-0 pb-lg-2 font-23"></span><span class="">Settings</span></a>												
												</div>
												<div class="col-6 align-items-center shadow-none text-center">											
													<a href="employment.html" class="text-dark p-4 last-slider-btn ctm-border-right"><span class="lnr lnr-user pr-0 pb-lg-2 font-23"></span><span class="">Profile</span></a>												
												</div>
											</div>
										</div>
									</div>
								</div>
								
								
								
							</aside>
						</div>
				
						<div class="col-xl-9 col-lg-8  col-md-12">
							<div class="quicklink-sidebar-menu ctm-border-radius shadow-sm bg-white card grow">
									<div class="card-body">
										<div class="flex-row list-group list-group-horizontal-lg" id="v-pills-tab" role="tablist" aria-orientation="vertical">
											<a class=" active list-group-item" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Overview</a>
											<a class="list-group-item" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Review Types</a>
										</div>
									</div>
								</div>
							<div class="card shadow-sm ctm-border-radius grow">
								<div class="card-header d-flex align-items-center justify-content-between">
									<h4 class="card-title mb-0 d-inline-block">Reviews</h4>
									<a href="create-review.html" class="btn btn-theme button-1 ctm-border-radius text-white float-right"><span></span> Create Review</a>
								</div>
								<div class="card-body align-center">
									<div class="tab-content" id="v-pills-tabContent">
									
									
										<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
											<div class="employee-office-table">
												<div class="table-responsive">
													<table class="table custom-table table-hover">
														<thead>
															<tr>
																<th>Review Name</th>
																<th>Reviewers</th>
																<th>Begin On</th>
																<th>Due By</th>
																<th>Status</th>
																<th>Actions</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>Monthly Review</td>
																<td>
																	<a href="employment.html" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
																	<h2><a href="employment.html"> Richard Wilson</a></h2>
																</td>
																<td>15 Dec 2019</td>
																<td>17 Dec 2019</td>
																<td>
																	<div class="dropdown action-label drop-active">
																		<a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> In Progress <i class="caret"></i></a>
																		<div class="dropdown-menu">
																			<a class="dropdown-item" href="javascript:void(0)"> In Progress</a>
																			<a class="dropdown-item" href="javascript:void(0)"> Completed</a>
																			
																		</div>
																	</div>
																</td>
																<td>
																	<div class="table-action">
																		<a href="edit-review.html" class="btn btn-sm btn-outline-success">
																			<span class="lnr lnr-pencil"></span> Edit
																		</a>
																		<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
																			<span class="lnr lnr-trash"></span> Delete
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Employees Review</td>
																<td>
																	<a href="employment.html" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
																	<h2><a href="employment.html"> Richard Wilson</a></h2>
																</td>
																<td>15 Dec 2019</td>
																<td>17 Dec 2019</td>
																<td>
																	<div class="dropdown action-label drop-active">
																		<a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> In Progress <i class="caret"></i></a>
																		<div class="dropdown-menu">
																			<a class="dropdown-item" href="javascript:void(0)"> In Progress</a>
																			<a class="dropdown-item" href="javascript:void(0)"> Completed</a>
																			
																		</div>
																	</div>
																</td>
																<td>
																	<div class="table-action">
																		<a href="edit-review.html" class="btn btn-sm btn-outline-success">
																			<span class="lnr lnr-pencil"></span> Edit
																		</a>
																		<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
																			<span class="lnr lnr-trash"></span> Delete
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Employees Review</td>
																<td>
																	<a href="employment.html" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
																	<h2><a href="employment.html"> Richard Wilson</a></h2>
																</td>
																<td>15 Dec 2019</td>
																<td>17 Dec 2019</td>
																<td>
																	<div class="dropdown action-label drop-active">
																		<a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> In Progress <i class="caret"></i></a>
																		<div class="dropdown-menu">
																			<a class="dropdown-item" href="javascript:void(0)"> In Progress</a>
																			<a class="dropdown-item" href="javascript:void(0)"> Completed</a>
																			
																		</div>
																	</div>
																</td>
																<td>
																	<div class="table-action">
																		<a href="edit-review.html" class="btn btn-sm btn-outline-success">
																			<span class="lnr lnr-pencil"></span> Edit
																		</a>
																		<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
																			<span class="lnr lnr-trash"></span> Delete
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Employees Review</td>
																<td>
																	<a href="employment.html" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
																	<h2><a href="employment.html"> Richard Wilson</a></h2>
																</td>
																<td>15 Dec 2019</td>
																<td>17 Dec 2019</td>
																<td>
																	<div class="dropdown action-label drop-active">
																		<a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> In Progress <i class="caret"></i></a>
																		<div class="dropdown-menu">
																			<a class="dropdown-item" href="javascript:void(0)"> In Progress</a>
																			<a class="dropdown-item" href="javascript:void(0)"> Completed</a>
																			
																		</div>
																	</div>
																</td>
																<td>
																	<div class="table-action">
																		<a href="edit-review.html" class="btn btn-sm btn-outline-success">
																			<span class="lnr lnr-pencil"></span> Edit
																		</a>
																		<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
																			<span class="lnr lnr-trash"></span> Delete
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Employees Review</td>
																<td>
																	<a href="employment.html" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
																	<h2><a href="employment.html"> Richard Wilson</a></h2>
																</td>
																<td>15 Dec 2019</td>
																<td>17 Dec 2019</td>
																<td>
																	<div class="dropdown action-label drop-active">
																		<a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> In Progress <i class="caret"></i></a>
																		<div class="dropdown-menu">
																			<a class="dropdown-item" href="javascript:void(0)"> In Progress</a>
																			<a class="dropdown-item" href="javascript:void(0)"> Completed</a>
																			
																		</div>
																	</div>
																</td>
																<td>
																	<div class="table-action">
																		<a href="edit-review.html" class="btn btn-sm btn-outline-success">
																			<span class="lnr lnr-pencil"></span> Edit
																		</a>
																		<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
																			<span class="lnr lnr-trash"></span> Delete
																		</a>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
										
                                        
										<div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
											<div class="employee-office-table">
												<div class="table-responsive">
													<table class="table custom-table table-hover">
														<thead>
															<tr>
																<th>Name</th>
																<th>Created By</th>
																<th>Scheduled For</th>
																<th>Actions</th>
															</tr>
														</thead>
														<tbody>
															
															<tr>
																<td>Monthly Review</td>
																<td>
																	<a href="employment.html" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
																	<h2><a href="employment.html"> Richard Wilson</a></h2>
																</td>
																<td>
																	Everyone
																</td>
																<td>
																	<div class="table-action">
																		<a href="edit-review.html" class="btn btn-sm btn-outline-success">
																			<span class="lnr lnr-pencil"></span> Edit
																		</a>
																		<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
																			<span class="lnr lnr-trash"></span> Delete
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Employees Review</td>
																<td>
																	<a href="employment.html" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
																	<h2><a href="employment.html"> Richard Wilson</a></h2>
																</td>
																<td>
																	Everyone
																</td>
																<td>
																	<div class="table-action">
																		<a href="edit-review.html" class="btn btn-sm btn-outline-success">
																			<span class="lnr lnr-pencil"></span> Edit
																		</a>
																		<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
																			<span class="lnr lnr-trash"></span> Delete
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Employees Review</td>
																<td>
																	<a href="employment.html" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
																	<h2><a href="employment.html"> Richard Wilson</a></h2>
																</td>
																<td>
																	Everyone
																</td>
																<td>
																	<div class="table-action">
																		<a href="edit-review.html" class="btn btn-sm btn-outline-success">
																			<span class="lnr lnr-pencil"></span> Edit
																		</a>
																		<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
																			<span class="lnr lnr-trash"></span> Delete
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Employees Review</td>
																<td>
																	<a href="employment.html" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
																	<h2><a href="employment.html"> Richard Wilson</a></h2>
																</td>
																<td>
																	Everyone
																</td>
																<td>
																	<div class="table-action">
																		<a href="edit-review.html" class="btn btn-sm btn-outline-success">
																			<span class="lnr lnr-pencil"></span> Edit
																		</a>
																		<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
																			<span class="lnr lnr-trash"></span> Delete
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Employees Review</td>
																<td>
																	<a href="employment.html" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
																	<h2><a href="employment.html"> Richard Wilson</a></h2>
																</td>
																<td>
																	Everyone
																</td>
																<td>
																	<div class="table-action">
																		<a href="edit-review.html" class="btn btn-sm btn-outline-success">
																			<span class="lnr lnr-pencil"></span> Edit
																		</a>
																		<a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
																			<span class="lnr lnr-trash"></span> Delete
																		</a>
																	</div>
																</td>
															</tr>
															
														</tbody>
													</table>
												</div>
											</div>
										</div>
										
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
    );
}

export default Review;